<?php
/*
Plugin Name: Project Post Type
Plugin URI: http://blog.aligoren.net
Description: Project Post Plugin (Custom Post Types)
Version: 0.1
Author: Ali GOREN
Author Email: goren.ali@yandex.com
License:

  Copyright 2011 Ali GOREN (goren.ali@yandex.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as 
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  
*/

// CPT ��in Buras�
add_action( 'init', 'projeler' );
function projeler() {
  register_post_type( 'projeler',
    array(
      'labels' => array(
        'name' => __( 'Projeler' ),
		'menu_name' => ( 'Projeler' ),
		'add_new' => ( 'Yeni Proje' ),
		'add_new_item' => ( 'Yeni Proje Ekle' ),
		'search_items' => ( 'Projelerde Ara' ),
        'singular_name' => __( 'Proje' )
      ),
      'public' => true,
      'has_archive' => true,
	  'supports' => array( 'title', 'editor', 'comments', 'post-templates'),
	  'taxonomies' => array( '' ),
	  'menu_icon' => plugins_url( 'project.png', __FILE__ ),
      'rewrite' => array('slug' => 'proje','with_front' => FALSE),
	    'capability_type' => 'post',
		'hierarchical' => false,
    )
  );
  // flush rewrite
  flush_rewrite_rules();
}

// Kategoriler
function proje_kategorileri() {
  $labels = array(
    'name'              => _x( 'Proje Kategorileri', 'taxonomy general name' ),
    'singular_name'     => _x( 'Proje Kategorisi', 'taxonomy singular name' ),
    'search_items'      => __( 'Proje Kategorilerinde Ara' ),
    'all_items'         => __( 'Proje Kategorileri' ),
    'parent_item'       => __( 'Alt Proje Kategorileri' ),
    'parent_item_colon' => __( 'Alt Proje Kategorisi:' ),
    'edit_item'         => __( 'Proje Kategorisini Duzenle' ), 
    'update_item'       => __( 'Proje Kategorisini Guncelle' ),
    'add_new_item'      => __( 'Yeni Proje Kategorisi Ekle' ),
    'new_item_name'     => __( 'Yeni Proje Kategorisi' ),
    'menu_name'         => __( 'Proje Kategorileri' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'proje_kategori', 'projeler', $args );
  flush_rewrite_rules();
}
add_action( 'init', 'proje_kategorileri', 0 );

// Meta Box
require_once("meta-box-class/my-meta-box-class.php");

/*
* configure your meta box
*/
$config = array(
    'id' => 'proje_meta_box',             // meta box id, unique per meta box 
    'title' => 'Projeler',      // meta box title
    'pages' => array( 'projeler'),    // post types, accept custom post types as well, default is array('post'); optional
    'context' => 'side',               // where the meta box appear: normal (default), advanced, side; optional
    'priority' => 'high',                // order of meta box: high (default), low; optional
    'fields' => array(),                 // list of meta fields (can be added by field arrays) or using the class's functions
    'local_images' => false,             // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => false            //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
);

$my_meta = new AT_Meta_Box($config);
$my_meta->addText($prefix.'proje_text_field_id',array('name'=> 'Proje Linki ', 'desc' => 'Linkler http ile baslamali
<br/><br/>Ornek: http://site.com<br/><br/>Thanks: <a href="http://en.bainternet.info/2012/how-i-add-a-wordpress-metabox" target="_blank">Ohad Raz</a>'));
$my_meta->Finish();
/*<?php echo get_post_meta($post->ID, 'proje_text_field_id', true); ?>*/

// Show Meta
function proje_meta_goster($proje_meta_icerik) {
	if (is_single()) {
		global $post;
		//echo get_post_meta($post->ID, 'proje_text_field_id', true);
		if(!filter_var(get_post_meta($post->ID, 'proje_text_field_id', true), FILTER_VALIDATE_URL))
		{
			$proje_meta_icerik .= get_the_content();
			return get_the_content();
		}
		$proje_meta_icerik .= '<a href="'.get_post_meta($post->ID, 'proje_text_field_id', true).' " target="_blank" rel="nofollow"><img src="'.plugin_dir_url('projeler').'/projeler/proje.png"></a></a>';
		return $proje_meta_icerik;
	}
	
}
add_filter('the_content', 'proje_meta_goster');
?>

